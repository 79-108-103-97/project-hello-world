import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./reducers/cart-reducer";
import favoritReducer from "./reducers/favorit-reducer";

let count = 0;

const logger = (store) => (next) => (action) => {
  console.log("action", action);

  let result = next(action);
  count++;
  
  localStorage.setItem("cart", JSON.stringify(store.getState().cart.products));
  localStorage.setItem(
    "favorit",
    JSON.stringify(store.getState().favorit.products)
  );

  console.log("Количество обработанных действий: ", count);
  console.log("next state", store.getState());
  return result;
};
export const store = configureStore({
  reducer: {
    cart: cartReducer,
    favorit: favoritReducer,
  },
  middleware: [logger],
});
