import { createSlice } from "@reduxjs/toolkit";

export const favoritSlice = createSlice({
  name: "favorit",
  initialState: {
    products: JSON.parse(localStorage.getItem("favorit")) || [],
  },
  reducers: {
    addProductFav: (prevState, action) => {
      const products = action.payload;
      const hasInFavorit = prevState.products.some(
        (prevProduct) => prevProduct.id === products.id
      );

      if (hasInFavorit) return prevState;
      return {
        ...prevState,
        products: [...prevState.products, action.payload],
      };
    },
    removeProductFav: (prevState, action) => {
      const products = action.payload;
      return {
        ...prevState,
        products: prevState.products.filter(
          (prevProduct) => prevProduct.id !== products.id
        ),
      };
    },
  },
});
export const { addProductFav, removeProductFav } = favoritSlice.actions;

export default favoritSlice.reducer;
