import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
  name: "cart",
  initialState: {
    products: JSON.parse(localStorage.getItem("cart")) || [],
  },
  reducers: {
    addProduct: (prevState, action) => {
      const products = action.payload;
      const hasInCart = prevState.products.some(
        (prevProduct) => prevProduct.id === products.id
      );
      if (hasInCart) return prevState;
      return {
        ...prevState,
        products: [...prevState.products, action.payload],
      };
    },
    removeProduct: (prevState, action) => {
      const products = action.payload;
      return {
        ...prevState,
        products: prevState.products.filter(
          (prevProduct) => prevProduct.id !== products.id
        ),
      };
    },
  },
});
export const { addProduct, removeProduct } = cartSlice.actions;

export default cartSlice.reducer;
