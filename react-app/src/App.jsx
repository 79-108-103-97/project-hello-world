import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import PageProduct from "./components/PageProduct/PageProduct";
import PageHome from "./components/PageHome/PageHome";
import PageNotFound from "./components/PageNotFound/PageNotFound";
import Breadcrumbs from "./components/Breadcrumbs/Breadcrumbs";

import items from "./data/items";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<PageHome />} />
        <Route
          path="/product"
          element={
            <>
              <Breadcrumbs items={items} />
              <PageProduct />
            </>
          }
        />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
