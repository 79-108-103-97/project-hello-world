const colors = [
  { id: 1, img: "/images/color-1.webp", value: "красный" },
  { id: 2, img: "/images/color-2.webp", value: "зелёный" },
  { id: 3, img: "/images/color-3.webp", value: "розовый" },
  { id: 4, img: "/images/color-4.webp", value: "синий" },
  { id: 5, img: "/images/color-5.webp", value: "белый" },
  { id: 6, img: "/images/color-6.webp", value: "черный" },
];
export default colors;
