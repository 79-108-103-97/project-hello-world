import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import "./Header.css";

function Header() {
  const countCart = useSelector((store) => store.cart.products.length);
  const countFavorit = useSelector((store) => store.favorit.products.length);
  return (
    <header className="market-header">
      <div className="container">
        <div className="market-header__inner">
          <Link to="/" className="link">
            <div className="logo">
              <img
                className="logo__img"
                src="./icons/favicon.svg"
                alt="Логотип"
              />
              <h1 className="logo__title title">
                <span className="logo__title-carrot">Мой</span>Маркет
              </h1>
            </div>
          </Link>
          <div className="market-header__icon-container">
            <div className="market-header__icon-love">
              <i className="market-header__icon icon-love"></i>
              {countFavorit !== 0 && (
                <span className="market-header__icon-count basket-count">
                  {countFavorit}
                </span>
              )}
            </div>
            <div className="market-header__icon-cart">
              <i className="market-header__icon icon-basket"></i>
              {countCart !== 0 && (
                <span className="market-header__icon-count basket-count">
                  {countCart}
                </span>
              )}
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
