import React from "react";

import "./Iframe.css";

function Iframe() {
  return (
    <iframe
      className="aside-advertising__iframe"
      title="Реклама"
      src="./ads/ads.html"
    >
      Ваш браузер не поддерживает плавающие фреймы!
    </iframe>
  );
}
export default Iframe;
