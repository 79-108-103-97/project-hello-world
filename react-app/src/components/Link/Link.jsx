import React from "react";

import "./Link.css";

function Link(props) {
  const { href, children } = props;
  return (
    <a className="link" href={href}>
      {children}
    </a>
  );
}
export default Link;
