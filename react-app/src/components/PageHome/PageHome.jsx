import React from "react";
import { Link } from "react-router-dom";

import styles from "./PageHome.module.css";

function PageHome() {
  return (
    <main className={styles.homePage}>
      <div className="container">
        <div className={styles.homePageInner}>
          <div>
            Здесь должно быть содержимое главной страницы.
            <br /> Но в рамках курса главная страница используется лишь в
            демонстрационных целях
          </div>
          <Link to="/product" className="link">
            Перейти на страницу товара
          </Link>
        </div>
      </div>
    </main>
  );
}
export default PageHome;
