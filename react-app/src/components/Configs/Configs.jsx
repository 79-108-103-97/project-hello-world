import React, { useState } from "react";
import ConfigButton from "./ConfigButton";

import configs from "../../data/configs";

import "./Configs.css";

function Configs() {
  let configChecked = configs[0].value;
  const [configCheck, setConfigCheck] = useState(configChecked);
  return (
    <div className="config-section section">
      <span className="config-section__title title">
        Конфигурация памяти: {configCheck}
      </span>
      <div className="config-section__items">
        {configs.map((config) => (
          <div
            key={config.id}
            className="config-section__config-item radio-btn"
          >
            <ConfigButton
              config={config}
              checked={configCheck === config.value}
              onChange={(event) => setConfigCheck(event.target.value)}
            />
          </div>
        ))}
      </div>
    </div>
  );
}
export default Configs;
