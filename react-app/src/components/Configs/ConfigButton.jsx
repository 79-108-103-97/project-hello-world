import React from "react";

function ConfigButton(props) {
  const { config, checked, onChange } = props;
  return (
    <>
      <input
        checked={checked}
        onChange={onChange}
        className="radio-btn__input"
        type="radio"
        name="config"
        value={config.value}
        id={"config_" + config.id}
      />
      <label
        className="config-section__label radio-btn__label  btn-light"
        htmlFor={"config_" + config.id}
      >
        {config.value}
      </label>
    </>
  );
}
export default ConfigButton;
