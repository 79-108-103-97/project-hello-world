import React, { useState } from "react";
import ColorButton from "./ColorButton";

import colors from "../../data/colors";

import "./Colors.css";

function Colors() {
  const colorChecked = colors[3].value;
  const [colorCheck, setColorCheck] = useState(colorChecked);
  return (
    <>
      <div className="color-section section">
        <span className="color-section__color-title title">
          Цвет товара: {colorCheck}
        </span>
      </div>
      <div className="color-section__color-items">
        {colors.map((color) => (
          <div key={color.id} className="color-section__color-item radio-btn">
            <ColorButton
              color={color}
              checked={colorCheck === color.value}
              onChange={(event) => setColorCheck(event.target.value)}
            />
          </div>
        ))}
      </div>
    </>
  );
}
export default Colors;
