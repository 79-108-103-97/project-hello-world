import React from "react";

function ColorButton(props) {
  const { color, checked, onChange } = props;
  return (
    <>
      <input
        checked={checked}
        onChange={onChange}
        className="radio-btn__input"
        value={color.value}
        type="radio"
        name="colors"
        id={"color_" + color.id}
      />
      <label
        style={{
          backgroundImage: `url(${color.img})`,
        }}
        className="color-section__label radio-btn__label btn-light"
        htmlFor={"color_" + color.id}
      ></label>
    </>
  );
}
export default ColorButton;
