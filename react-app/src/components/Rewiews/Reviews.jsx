import React from "react";
import reviews from "../../data/reviews";
import Review from "./Review";
import "./Reviews.css";

function Reviews() {
  return (
    <div className="reviews-section section">
      <div className="reviews-section__top">
        <div className="reviews-section__top-left">
          <span className="reviews-section__top-title title">Отзывы</span>
          <span className="reviews-section__top-count font-grey">
            {reviews.length}
          </span>
        </div>
      </div>
      <div className="reviews-section__list">
        {reviews.map((review, index) => (
          <React.Fragment key={review.id}>
            <div className="review-item">
              <Review review={review} />
            </div>
            {reviews.length !== review.id && (
              <div className="reviews-section__separator"></div>
            )}
          </React.Fragment>
        ))}
      </div>
    </div>
  );
}
export default Reviews;
