import React from "react";

function Review(props) {
  const { review } = props;
  const stars = [];
  for (let i = 0; i < review.stars; i++) {
    stars.push(<span key={i} className="star icon-Star"></span>);
  }
  for (let i = review.stars; i < 5; i++) {
    stars.push(<span key={i} className="star star-dark icon-Star"></span>);
  }
  return (
    <>
      <div className="review-item__container-img">
        <img
          className="review-item__img"
          src={review.img}
          alt="Фото Валерий Коваленко"
        />
      </div>
      <div className="review-item__container-content">
        <div className="review-item__top">
          <h4 className="review-item__top-title">{review.name}</h4>
          <div className="container-stars">
            {stars}
          </div>
        </div>
        <div className="review-item__info">
          <p>
            <span className="font-bold">Опыт использования: </span>
            {review.userExperience}
          </p>
          <p>
            <span className="font-bold">Достоинства:</span>
            <br />
            {review.fdvantages}
          </p>
          <p>
            <span className="font-bold">Недостатки:</span>
            <br />
            {review.flaws}
          </p>
        </div>
      </div>
    </>
  );
}
export default Review;
