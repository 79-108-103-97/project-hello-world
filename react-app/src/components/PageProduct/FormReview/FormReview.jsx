import React, { useState } from "react";

import "./FormReview.css";

function FormReview() {
  const [name, setName] = useState(
    localStorage.getItem("userName") ? localStorage.getItem("userName") : ""
  );
  const [grade, setGrade] = useState(
    localStorage.getItem("userGrade") ? localStorage.getItem("userGrade") : ""
  );
  const [nameError, setNameError] = useState("");
  const [gradeError, setGradeError] = useState("");

  const hundlerSubmit = (e) => {
    e.preventDefault();
    const formValid = validate();
    if (formValid) {
      localStorage.removeItem("userName");
      localStorage.removeItem("userGrade");
      e.target.submit();
      alert(
        "Ваш отзыв был успешно отправлен и будет отображён после модерации"
      );
    }
  };

  const validate = () => {
    if (name.length === 0) {
      setNameError("Вы забыли указать имя и фамилию");
      return false;
    } else if (name.length < 2) {
      setNameError("Имя не может быть короче 2-хсимволов");
      return false;
    } else {
      setNameError("");
    }
    if (
      !Number.isInteger(+grade) ||
      grade.length === 0 ||
      grade < 1 ||
      grade > 5
    ) {
      setGradeError("Оценка должна быть от 1 до 5");
      return false;
    } else {
      setGradeError("");
    }
    return true;
  };

  const hundlerName = (e) => {
    setName(e.target.value);
    localStorage.setItem("userName", e.target.value);
  };
  const hundlerGrade = (e) => {
    setGrade(e.target.value);
    localStorage.setItem("userGrade", e.target.value);
  };

  return (
    <div className="rewiews-add-section section">
      <div className="rewiews-add-section__inner">
        <span className="font-bold">Добавить свой отзыв</span>
        <form className="rewiews-add-form" id="form" onSubmit={hundlerSubmit}>
          <div className="rewiews-add-form__line-one">
            <div className="rewiews-add-form__people-name form-control-container">
              <input
                className="rewiews-add-form__people-name-input form-control"
                value={name}
                onInput={hundlerName}
                name="name"
                placeholder="Имя и фамилия"
              />
              {nameError && (
                <small className="form-control__error">{nameError}</small>
              )}
            </div>
            <div className="form-control-container">
              <input
                className="form-control"
                value={grade}
                onInput={hundlerGrade}
                name="grade"
                placeholder="Оценка"
              />
              {gradeError && (
                <small className="form-control__error">{gradeError}</small>
              )}
            </div>
          </div>
          <div className="rewiews-add-form__line-two">
            <textarea
              className="rewiews-add-form__textarea form-control form-control__textarea"
              name=""
              placeholder="Текст отзыва"
            ></textarea>
          </div>
          <div className="rewiews-add-form__footer">
            <button
              className="rewiews-add-form__submit btn-carrot"
              type="submit"
            >
              Отправить отзыв
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default FormReview;
