import React from "react";

import Sidebar from "./Sidebar/Sidebar";
import Configs from "../Configs/Configs";
import Colors from "../Colors/Colors";
import Reviews from "../Rewiews/Reviews";
import FormReview from "./FormReview/FormReview";

import "./PageProduct.css";
import TableCompar from "./TableCompar/TableCompar";

function PageProduct() {
  return (
    <main className="product-page">
      <div className="product-page__container container">
        <div className="product-page-top">
          <h2 className="product-page-top__title title">
            Смартфон Apple iPhone 13, синий
          </h2>
        </div>
        <div className="product-page-gallery">
          <img
            className="product-page-gallery__item"
            src="./images/image-1.webp"
            alt="Фото товара 1"
          />
          <img
            className="product-page-gallery__item"
            src="./images/image-2.webp"
            alt="Фото товара 2"
          />
          <img
            className="product-page-gallery__item"
            src="./images/image-3.webp"
            alt="Фото товара 3"
          />
          <img
            className="product-page-gallery__item"
            src="./images/image-4.webp"
            alt="Фото товара 4"
          />
          <img
            className="product-page-gallery__item"
            src="./images/image-5.webp"
            alt="Фото товара 5"
          />
        </div>
        <div className="product-page-content">
          <div className="product-info">
            <Colors />
            <Configs />
            <div className="charact-section section">
              <span className="charact-section__title title">
                Характеристики товара
              </span>
              <ul className="charact-section__items">
                <li className="charact-section__item">
                  Экран: <span className="font-bold">6.1</span>
                </li>
                <li className="charact-section__item">
                  Встроенная память: <span className="font-bold">128 ГБ</span>
                </li>
                <li className="charact-section__item">
                  Операционная система:{" "}
                  <span className="font-bold">iOS 15</span>
                </li>
                <li className="charact-section__item">
                  Беспроводные интерфейсы:{" "}
                  <span className="font-bold">NFC, Bluetooth, Wi-Fi</span>
                </li>
                <li className="charact-section__item">
                  Процессор:{" "}
                  <span>
                    <a
                      className="font-bold link"
                      rel="noreferrer"
                      href="https://ru.wikipedia.org/wiki/Apple-A15"
                      target="_blank"
                    >
                      Apple A15 Bionic
                    </a>
                  </span>
                </li>
                <li className="charact-section__item">
                  Вес: <span className="font-bold">173 г</span>
                </li>
              </ul>
            </div>
            <div className="descript-section section">
              <span className="descript-section__title title">Описание</span>
              <p className="descript-section__item">
                Наша самая совершенная система двух камер. <br />
                Особый взгляд на прочность дисплея. <br />
                Чип, с которым всё супер быстро. <br />
                Аккумулятор держится заметно дольше. <br />
                <i>iPhone 13 - сильный мира всего.</i>
              </p>
              <p className="descript-section__item">
                Мы разработали совершенно новую схему расположения и развернули
                объективы на 45 градусов. Благодаря этому внктри корпуса
                поместилась наша лучшая система двух камер с увеличенной
                матрицей широкоугольной камеры. Кроме того, мы освободили место
                для системы оптической стабилизации изображения сдвигом матрицы.
                И повысили скорость работы матрицы на сверхширокоугольной
                камере.
              </p>
              <p className="descript-section__item">
                Новая сверхширокоугольная камера видит больше деталей в тёмных
                областях снимков. Новая широкоугольная камера улавливает на 47%
                больше света для более качественных фотографий и видео. Новая
                оптическая стабилизация со сдвигом матрицы обеспечит чёткие
                кадры даже в неустойчивом положении.
              </p>
              <p className="descript-section__item">
                Режим «Киноэффект» автоматически добавляет великолепные эффекты
                перемещенияфокуса и изменения резкости. Просто начните запись
                видео. Режим «Киноэффект»будет удерживать фокус на объекте
                съёмки, создавая красивый эффект размытиявокруг него. Режим
                «Киноэффект» распознаёт, когда нужно перевести фокус на
                другогочеловека или объект, который появился в кадре. Теперь
                ваши видео будут смотретьсякак настоящее кино.
              </p>
            </div>
            <div className="compar-section section">
              <span className="compar-section__title title">
                Сравнение моделей
              </span>
              <TableCompar />
            </div>
            <Reviews />
            <FormReview />
          </div>
          <Sidebar />
        </div>
      </div>
    </main>
  );
}

export default PageProduct;
