import React, { useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addProduct, removeProduct } from "../../../reducers/cart-reducer";
import {
  addProductFav,
  removeProductFav,
} from "../../../reducers/favorit-reducer";
import Ads from "./Ads/Ads";

import "./Sidebar.css";

function Sidebar() {
  const product = { id: 1, name: "IPhone 13" };

  const productsInCart = useSelector((store) => store.cart.products);
  const productsInFavorit = useSelector((store) => store.favorit.products);

  const hasInCart = useMemo(
    () => productsInCart.some((prevProduct) => prevProduct.id === product.id),
    [productsInCart, product.id]
  );
  const hasInFavorit = useMemo(
    () =>
      productsInFavorit.some((prevProduct) => prevProduct.id === product.id),
    [productsInFavorit, product.id]
  );

  const [inCard, setInCard] = useState(hasInCart);
  const dispatchCart = useDispatch();
  const [inFavorit, setInFavorit] = useState(hasInFavorit);
  const dispatchFavorit = useDispatch();

  const hundlerClickCard = () => {
    if (hasInCart) {
      const action = removeProduct(product);
      dispatchCart(action);
    } else {
      const action = addProduct(product);
      dispatchCart(action);
    }
    setInCard(!inCard);
  };

  const hundlerClickFavorit = () => {
    if (hasInFavorit) {
      const action = removeProductFav(product);
      dispatchFavorit(action);
    } else {
      const action = addProductFav(product);
      dispatchFavorit(action);
    }
    setInFavorit(!inFavorit);
  };

  return (
    <aside className="aside-container">
      <div className="aside-base-info">
        <div className="aside-base-info__price">
          <div className="aside-base-info__old-priсe-container">
            <div className="aside-base-info__old-priсe">
              <span className="aside-base-info__old-priсe-text">75 990₽</span>
            </div>
            <div className="aside-base-info__sale">
              <span>-8%</span>
            </div>
            <i
              onClick={hundlerClickFavorit}
              className={
                "aside-base-info__icon-love icon-love" +
                (inFavorit ? "-fill" : "")
              }
            ></i>
          </div>
          <div className="aside-base-info__actual-priсe-container">
            <span className="aside-base-info__actual-priсe-text">67 990₽</span>
          </div>
        </div>
        <div className="aside-base-info__delivery">
          <div className="aside-base-info__self-collection">
            <span className="aside-base-info__self-collection-text">
              Самовывоз в четверг, 1 сентября —
              <span className="aside-base-info__self-collection-price font-bold">
                бесплатно
              </span>
            </span>
          </div>
          <div className="aside-base-info__courier">
            <span className="aside-base-info__courier-text">
              Курьером в четверг, 1 сентября —
              <span className="aside-base-info__courier-price font-bold">
                бесплатно
              </span>
            </span>
          </div>
        </div>
        <button
          className={
            "aside-base-info__add-to-card " +
            (!inCard ? "btn-carrot" : "btn-grey")
          }
          onClick={hundlerClickCard}
          data-id="1"
        >
          <i className="aside-base-info__add-to-card-icon icon-basket"></i>
          {!inCard ? "Добавить в корзину" : "Товар уже в корзине"}
        </button>
      </div>
      <Ads />
    </aside>
  );
}

export default Sidebar;
