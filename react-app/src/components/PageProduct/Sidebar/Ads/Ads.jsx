import React from "react";
import Iframe from "../../../Iframe/Iframe";

import "./Ads.css";

function Ads() {
  return (
    <div className="aside-advertising">
      <div className="aside-advertising__title">
        <span className="aside-advertising__text">Реклама</span>
      </div>
      <div className="aside-advertising__container-iframes">
        <Iframe />
        <Iframe />
      </div>
    </div>
  );
}
export default Ads;
