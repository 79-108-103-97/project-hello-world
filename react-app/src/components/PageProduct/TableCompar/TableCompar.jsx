import React from "react";

import "./TableCompar.css";

function TableCompar() {
  return (
    <table className="compar-section__table">
      <thead>
        <tr className="compar-section__tr">
          <th className="compar-section__th">Модель</th>
          <th className="compar-section__th">Вес</th>
          <th className="compar-section__th">Высота</th>
          <th className="compar-section__th">Ширина</th>
          <th className="compar-section__th">Толщина</th>
          <th className="compar-section__th">Чип</th>
          <th className="compar-section__th">Объём памяти</th>
          <th className="compar-section__th">Аккумулятор</th>
        </tr>
      </thead>
      <tbody>
        <tr className="compar-section__tr">
          <td className="compar-section__td">Iphone11</td>
          <td className="compar-section__td">194 грамма</td>
          <td className="compar-section__td">150.9 мм</td>
          <td className="compar-section__td">75.7 мм</td>
          <td className="compar-section__td">8.3 мм</td>
          <td className="compar-section__td">A13 Bionic chip</td>
          <td className="compar-section__td">до 128 Гб</td>
          <td className="compar-section__td">до 17 часов</td>
        </tr>
        <tr className="compar-section__tr">
          <td className="compar-section__td">Iphone12</td>
          <td className="compar-section__td">164 грамма</td>
          <td className="compar-section__td">146.7 мм</td>
          <td className="compar-section__td">71.5 мм</td>
          <td className="compar-section__td">7.4 мм</td>
          <td className="compar-section__td">A14 Bionic chip</td>
          <td className="compar-section__td">до 256 Гб</td>
          <td className="compar-section__td">до 19 часов</td>
        </tr>
        <tr className="compar-section__tr">
          <td className="compar-section__td">Iphone13</td>
          <td className="compar-section__td">174 грамма</td>
          <td className="compar-section__td">146,7 мм</td>
          <td className="compar-section__td">71.5 мм</td>
          <td className="compar-section__td">7.65 мм</td>
          <td className="compar-section__td">A15 Bionicchip</td>
          <td className="compar-section__td">до 512 Гб</td>
          <td className="compar-section__td">до 19 часов</td>
        </tr>
      </tbody>
    </table>
  );
}
export default TableCompar;
