import React from "react";

import styled from "styled-components";

import { Link } from "react-router-dom";

const MainContainer = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
`;

const PageNotFoundInner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  gap: 20px;
`;

const PageNotFoundTitle = styled.div`
  font-size: 32px;
`;

const PageNotFoundText = styled.div`
  color: var(--color-dark-grey);
`;

function PageNotFound() {
  return (
    <MainContainer>
      <div className="container">
        <PageNotFoundInner>
          <PageNotFoundTitle>Ой! Страница не найдена</PageNotFoundTitle>
          <PageNotFoundText>
            Возможно, она была перемещена, или вы просто
            <br /> неверно указали адрес страницы.
          </PageNotFoundText>
          <Link to="/product" className="link">
            Перейти на страницу товара
          </Link>
        </PageNotFoundInner>
      </div>
    </MainContainer>
  );
}
export default PageNotFound;
