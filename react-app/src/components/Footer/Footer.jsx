import React from "react";

import { useCurrentDate } from "@kundinos/react-hooks";

import Link from "../Link/Link";
import "./Footer.css";

function Footer() {
  const currentDate = useCurrentDate({ every: "day" });
  const fullYear = currentDate.getFullYear();

  return (
    <footer className="market-footer">
      <div className="market-footer__inner container">
        <div className="copyright">
          <p className="font-bold">
            © ООО «<span className="font-bold font-carrot">Мой</span>Маркет»,
            2018-{fullYear}.
          </p>
          <p>
            Для уточненияинформации звоните по номеру
            <Link href="tel:+79000000000"> +7 900 000 0000</Link>,
          </p>
          <p>
            а предложения по сотрудничеству отправляйте на почту
            <Link href="mailto:partner@mymarket.com">
              {" "}
              partner@mymarket.com
            </Link>
          </p>
        </div>
        <Link href="#top">Наверх</Link>
      </div>
    </footer>
  );
}

export default Footer;
