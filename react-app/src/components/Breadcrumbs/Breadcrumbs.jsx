import React from "react";
import Link from "../Link/Link";

import "./Breadcrumbs.css";

function Breadcrumbs(props) {
  const { items } = props;
  return (
    <nav className="market-nav">
      <div className="container">
        <div className="breadcrumbs">
          {items.map((item) => (
            <React.Fragment key={item.id}>
              <Link href={item.href}>{item.name}</Link>
              {items.length !== item.id && <span>&#62;</span>}
            </React.Fragment>
          ))}
        </div>
      </div>
    </nav>
  );
}
export default Breadcrumbs;
