"use script";
const form = document.querySelector(".rewiews-add-form"),
  formInputs = form.querySelectorAll(".form-control"),
  inputName = form.querySelector(".form-control__name"),
  inputGrade = form.querySelector(".form-control__grade"),
  inputTextarea = form.querySelector(".form-control__textarea"),
  elemErrorName = form.querySelector(".form-control__error-name"),
  elemErrorGrade = form.querySelector(".form-control__error-grade"),
  buttonSubmit = form.querySelector(".rewiews-add-form__submit"),
  buttonAddToCart = document.querySelector(".aside-base-info__add-to-card"),
  countHTML = document.querySelector(".basket-count");
let id = buttonAddToCart.getAttribute("data-id");

/**
 * Проверка правильности отображения товаров в HTML документе
 */
function checkoutHTMLItems() {
  if (countItems() === 0) {
    countHTML.style.display = "none";
  } else {
    countHTML.style.display = "flex";
    countHTML.innerHTML = countItems();
  }
  if (checkIdInCart(id)) {
    buttonAddToCart.innerHTML =
      '<i class="aside-base-info__add-to-card-icon icon-basket"></i>Товар уже в корзине';
    buttonAddToCart.classList.remove("btn-carrot");
    buttonAddToCart.classList.add("btn-grey");
  } else {
    buttonAddToCart.innerHTML =
      '<i class="aside-base-info__add-to-card-icon icon-basket"></i>Добавить в корзину';
    buttonAddToCart.classList.add("btn-carrot");
    buttonAddToCart.classList.remove("btn-grey");
  }
}

checkoutHTMLItems();
buttonAddToCart.addEventListener("click", () => {
  if (checkIdInCart(id)) {
    removeFromCart(id);
    checkoutHTMLItems();
  } else {
    addToCart(id);
    checkoutHTMLItems();
  }
});

document.addEventListener("DOMContentLoaded", () => {
  inputName.addEventListener("input", () => {
    localStorage.setItem("userName", inputName.value);
  });
  inputGrade.addEventListener("input", () => {
    localStorage.setItem("userGrade", inputGrade.value);
  });
  inputTextarea.addEventListener("input", () => {
    localStorage.setItem("userTextarea", inputTextarea.value);
  });

  inputName.value = localStorage.getItem("userName");
  inputGrade.value = localStorage.getItem("userGrade");
  inputTextarea.value = localStorage.getItem("userTextarea");
});

buttonSubmit.addEventListener("click", (event) => {
  event.preventDefault();
  let empryImputs = Array.from(formInputs).filter(
    (input) => input.value === ""
  );

  formInputs.forEach((input) => {
    if (input.value === "") {
      input.classList.add("form-control_invalid");
    } else {
      input.classList.remove("form-control_invalid");
    }
  });

  if (empryImputs.length !== 0) {
    if (empryImputs[0] == inputName) {
      elemErrorName.innerHTML = "Вы забыли указать имя и фамилию";
      elemErrorName.classList.add("form-control__error_visible");
    } else {
      elemErrorName.classList.remove("form-control__error_visible");
    }
    if (empryImputs[0] == inputGrade) {
      elemErrorGrade.innerHTML = "Оценка должна быть от 1 до 5";
      elemErrorGrade.classList.add("form-control__error_visible");
    } else {
      elemErrorGrade.classList.remove("form-control__error_visible");
    }
    return false;
  }
  let inputNameValue = inputName.value;
  let inputGradeValue = inputGrade.value;

  if (inputNameValue.length < 2) {
    inputName.classList.add("form-control_invalid");
    elemErrorName.innerHTML = "Имя не может быть короче 2-хсимволов";
    elemErrorName.classList.add("form-control__error_visible");
    return false;
  } else {
    inputName.classList.remove("form-control_invalid");
    elemErrorName.classList.remove("form-control__error_visible");
  }
  if (
    Number.isInteger(+inputGradeValue) ||
    inputGradeValue > 5 ||
    inputGradeValue < 1
  ) {
    inputGrade.classList.add("form-control_invalid");
    elemErrorGrade.innerHTML = "Оценка должна быть от 1 до 5";
    elemErrorGrade.classList.add("form-control__error_visible");
    return false;
  } else {
    elemErrorGrade.classList.remove("form-control__error_visible");
    elemErrorGrade.classList.remove("form-control__error_visible");
  }

  localStorage.clear();
  form.submit();
});

/*Работа с корзиной */

/**
 * Добавление продукта в корзину
 * @param {Number} id
 * @returns
 */
function addToCart(id) {
  if (!isNaN(parseFloat(id)) && isFinite(id)) {
    let cart = {};
    cart = getCart();
    if (!cart.hasOwnProperty(id)) {
      cart[id] = 1;
    }
    // if (Object.keys(cart).length !== 0 && cart.hasOwnProperty(id)) {
    //   cart[id]++;
    // } else {
    // cart[id] = 1;
    // }

    localStorage.setItem("cart", JSON.stringify(cart));
  }
  return countItems();
}

/**
 * Удаление продукта из корзины
 * @param {Number} id
 * @returns
 */
function removeFromCart(id) {
  let cart = getCart();
  if (cart.hasOwnProperty(id)) {
    delete cart[id];
  }
  localStorage.setItem("cart", JSON.stringify(cart));
  return countItems();
}

/**
 * Получение продуктов из хранилища корзины
 * @returns
 */
function getCart() {
  if (localStorage.getItem("cart") !== null) {
    return JSON.parse(localStorage.getItem("cart"));
  }
  return {};
}

/**
 * Проверка наличия продукта в хранилище корзины
 * @param {Number} id
 * @returns
 */
function checkIdInCart(id) {
  let cart = getCart();
  return cart.hasOwnProperty(id);
}
/**
 * Получение количества продуктов в хранилище корзины
 * @returns
 */
function countItems() {
  if (localStorage.getItem("cart") !== null) {
    let cart = JSON.parse(localStorage.getItem("cart"));
    return Object.keys(cart).length;
  } else {
    return 0;
  }
}
