"use script";

//Упражнение 1

/**
 * Возвращает false если у объекта есть хотя бы одно свойство
 * @param {Object} obj
 * @returns {boolean}
 */
function isEmpty(obj) {
  for (var prop in obj) {
    return false;
  }
  return true;
}

let user = {};
console.log("Нет свойств: " + isEmpty(user));
user.age = 12;
console.log("Одно свойство: " + isEmpty(user));
user.name = "Olga";
console.log("Два свойства: " + isEmpty(user));

//Упражнение 3
let salaries = { John: 100000, Ann: 160000, Pete: 130000 };

/**
 * Производит повышение зарплаты на определенный
процент и возвращает объект с новыми зарплатами
 * @param {number} perzent процент повышения зп
 * @returns {Object} 
 */
function raiseSalary(perzent) {
  for (let key in salaries) {
    salaries[key] = Math.floor((salaries[key] / 100) * perzent + salaries[key]);
  }
  return salaries;
}
console.log("Повышение зп на 5%: ");
console.log(raiseSalary(5));
let sum = 0;
for (let key in salaries) {
  sum += salaries[key];
}
console.log("Сумма зарплат: " + sum);
