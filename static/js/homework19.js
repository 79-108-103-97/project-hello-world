"use script";

// Упражнение 1

let a = "$100";
let b = "300$";
let summ = Number(a.replace("$", "")) + Number(b.replace("$", ""));
console.log(summ); // Должно быть 400

// Упражнение 2

let message1 = " привет, медвед      ";
message1 = message1.trim();
message1 = message1.charAt(0).toUpperCase() + message1.slice(1);
console.log(message1); // “Привет, медвед”

// Упражнение 3

let age = prompt("Сколько вам лет?");
if (!isNaN(parseFloat(age)) || isFinite(age)) {
  age = Math.floor(age);
  let maturity = "";
  if (age >= 0 && age <= 3) {
    maturity = "младенец";
  } else if (age >= 4 && age <= 11) {
    maturity = "ребенок";
  } else if (age >= 12 && age <= 18) {
    maturity = "подросток";
  } else if (age >= 19 && age <= 40) {
    maturity = "познаёте жизнь";
  } else if (age >= 41 && age <= 80) {
    maturity = "познали жизнь";
  } else if (age >= 81) {
    maturity = "долгожитель";
  }
alert(`Вам ${age} лет/год и вы ${maturity}`);
} else{ alert("Неверное значение.");}

// Упражнение 4

let message = "Я работаю со строками как профессионал!";
let count = message.split(" ").length;
console.log(count); // Должно быть 6
