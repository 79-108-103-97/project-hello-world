"use script";

//Упражнение 1
function getSumm(arr) {
  let sum = 0;
  arr.forEach((element) => {
    if (!isNaN(parseFloat(element)) && isFinite(element)) {
      sum += element;
    }
  });
  return sum;
}

let arr1 = [1, 2, 10, 5];
alert(getSumm(arr1)); // 18
let arr2 = ["a", {}, 3, 3, -2];
alert(getSumm(arr2)); // 4

//Упражнение 3
let cart = [];

/**
 * Добававляет id товара в массив корзины
 * @param {Number} id
 * @returns
 */
function addToCart(id) {
  if (!isNaN(parseFloat(id)) && isFinite(id) && !cart.includes(id)) {
    cart.push(id);
    return cart;
  }
  return false;
}
/**
 * Удаляет id товара из массива корзины
 * @param {Number} id
 * @returns
 */
function removeFromCart(id) {
  if (cart.includes(id)) {
    cart.splice(cart.indexOf(id), 1);
    return cart;
  }
  return false;
}

// В корзине один товар
cart = [4884];

// Добавили товар
addToCart(3456);
console.log(cart); // [4884, 3456]

// Повторно добавили товар
addToCart(3456);
console.log(cart); // [4884, 3456]

// Удалили товар
removeFromCart(4884);
console.log(cart); // [3456]
