"use script";

class Form {
  constructor(form) {
    this.form = form;
  }
  validator() {}
  submit() {
    localStorage.clear();
    alert("Форма успешно отправлена!");
    form.submit();
  }
}

class AddReviewForm extends Form {
  constructor(
    form,
    formInputs,
    inputName,
    inputGrade,
    inputTextarea,
    elemErrorName,
    elemErrorGrade
  ) {
    super();
    this.form = form;
    this.formInputs = formInputs;
    this.inputName = inputName;
    this.inputGrade = inputGrade;
    this.inputTextarea = inputTextarea;
    this.elemErrorName = elemErrorName;
    this.elemErrorGrade = elemErrorGrade;
  }

  fillingPreservation() {
    this.inputName.addEventListener("input", () => {
      localStorage.setItem("userName", this.inputName.value);
    });
    this.inputGrade.addEventListener("input", () => {
      localStorage.setItem("userGrade", this.inputGrade.value);
    });
    this.inputTextarea.addEventListener("input", () => {
      localStorage.setItem("userTextarea", this.inputTextarea.value);
    });
    this.inputName.value = localStorage.getItem("userName");
    this.inputGrade.value = localStorage.getItem("userGrade");
    this.inputTextarea.value = localStorage.getItem("userTextarea");
  }
  validator() {
    let empryImputs = Array.from(this.formInputs).filter(
      (input) => input.value === ""
    );

    this.formInputs.forEach((input) => {
      if (input.value === "") {
        input.classList.add("form-control_invalid");
      } else {
        input.classList.remove("form-control_invalid");
      }
    });

    if (empryImputs.length !== 0) {
      if (empryImputs[0] == this.inputName) {
        this.elemErrorName.innerHTML = "Вы забыли указать имя и фамилию";
        this.elemErrorName.classList.add("form-control__error_visible");
      } else {
        this.elemErrorName.classList.remove("form-control__error_visible");
      }
      if (empryImputs[0] == this.inputGrade) {
        this.elemErrorGrade.innerHTML = "Оценка должна быть от 1 до 5";
        this.elemErrorGrade.classList.add("form-control__error_visible");
      } else {
        this.elemErrorGrade.classList.remove("form-control__error_visible");
      }
      return false;
    }

    let inputNameValue = this.inputName.value;
    let inputGradeValue = this.inputGrade.value;

    if (this.checkoutName(inputNameValue)) {
      this.inputName.classList.remove("form-control_invalid");
      this.elemErrorName.classList.remove("form-control__error_visible");
    } else {
      this.inputName.classList.add("form-control_invalid");
      this.elemErrorName.innerHTML = "Имя не может быть короче 2-хсимволов";
      this.elemErrorName.classList.add("form-control__error_visible");
      return false;
    }
    if (this.checkoutGrade(inputGradeValue)) {
      this.elemErrorGrade.classList.remove("form-control__error_visible");
      this.elemErrorGrade.classList.remove("form-control__error_visible");
    } else {
      this.inputGrade.classList.add("form-control_invalid");
      this.elemErrorGrade.innerHTML = "Оценка должна быть от 1 до 5";
      this.elemErrorGrade.classList.add("form-control__error_visible");
      return false;
    }
    return true;
  }

  checkoutName(value) {
    if (value.length < 2) {
      return false;
    }
    return true;
  }
  checkoutGrade(value) {
    if (!Number.isInteger(+value) || value > 5 || value < 1) {
      return false;
    }
    return true;
  }
}

const buttonSubmit = document.querySelector(".rewiews-add-form__submit");
let reviewForm = new AddReviewForm(
  document.querySelector(".rewiews-add-form"),
  this.form.querySelectorAll(".form-control"),
  this.form.querySelector(".form-control__name"),
  this.form.querySelector(".form-control__grade"),
  this.form.querySelector(".form-control__textarea"),
  this.form.querySelector(".form-control__error-name"),
  this.form.querySelector(".form-control__error-grade")
);

buttonSubmit.addEventListener("click", (event) => {
  event.preventDefault();

  console.log(reviewForm.validator());
  if (reviewForm.validator()) {
    reviewForm.submit();
  }
});
document.addEventListener("DOMContentLoaded", () => {
  reviewForm.fillingPreservation();
});
