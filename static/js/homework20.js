"use script";

// Упражнение 1
let numbers = "Четные числа от 0 до 20: ";
for (let i = 0; i <= 20; i++) {
  if (i % 2 == 0) {
    numbers += `${i} `;
  }
}
console.log(numbers);

//Упражнение 2
let sum = 0;
let errors = "";
for (let i = 0; i < 3; i++) {
  let number = prompt("Введите число");
  if (!isNaN(parseFloat(number)) && isFinite(number)) {
    sum += +number;
  } else {
    errors = "Ошибка, вы ввели не число";
    alert(errors);
    break;
  }
}
if (!errors) alert(`Сумма - ${sum}`);

//Упражнение 3
let numberMonth = prompt("номер месяца");
if (!isNaN(parseFloat(numberMonth)) && isFinite(numberMonth)) {
  alert(getNameOfMonth(numberMonth));
} else {
  alert("Ошибка, вы ввели не число");
}
function getNameOfMonth(month) {
  switch (+month) {
    case 0:
      return "Январь";
    case 1:
      return "Февраль";
    case 2:
      return "Март";
    case 3:
      return "Апрель";
    case 4:
      return "Май";
    case 5:
      return "Июнь";
    case 6:
      return "Июль";
    case 7:
      return "Август";
    case 8:
      return "Сентябрь";
    case 9:
      return "Октябрь";
    case 10:
      return "Ноябрь";
    case 11:
      return "Декабрь";
    default:
      return "Кажется, такого не существует";
  }
}
let allMonth = "Все месяцы, кроме октября: ";
for (let i = 0; i < 12; i++) {
  if (i !== 9) allMonth += getNameOfMonth(i) + " ";
}
console.log(allMonth);

//Упражнение 4
/*  Что такое IIFE?
 
Это выражение немедленного вызова функции, функция JavaScript, 
которая запускается сразу после определения.
*/

/* Прим 

(function IIFE(msg, times) {
    for (var i = 1; i <= times; i++) {
        console.log(msg);
    }
}("Hello!", 5));*/

/* или

((msg, times) => {
  for (var i = 1; i <= times; i++) {
    console.log(msg);
  }
})("Hello!", 5);*/
