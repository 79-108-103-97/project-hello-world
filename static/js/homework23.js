"use script";

//Упражнение 1

let seconds = +prompt("Введите количество минут.");

let promiseTimer = new Promise((resolve, reject) => {
  if (Number.isInteger(seconds) && seconds > 0) {
    let timer = setInterval(() => {
      if (seconds > 0) {
        console.log("Осталось " + seconds);
        seconds--;
      } else {
        clearInterval(timer);
        resolve();
      }
    }, 1000);
  } else {
    reject(seconds);
  }
})
  .then(() => {
    console.log("Время кончилось!");
  })
  .catch((err) => {
    console.log("Введено неверное значение! " + err);
  });

//Упражнение 2

let start = Date.now();

fetch("https://reqres.in/api/users")
  .then((response) => {
    return response.json();
  })
  .then((result) => {
    let strData = "Получили пользователей: " + result.data.length + "\n";
    result.data.forEach((element) => {
      strData +=
        "- " +
        element["first_name"] +
        " " +
        element["last_name"] +
        " (" +
        element["email"] +
        ")\n";
    });
    let finish = Date.now() - start;
    console.log(strData, "Time: ", finish / 1000, " sec.");
  })
  .catch((err) => {
    console.log("Error", err);
  });
