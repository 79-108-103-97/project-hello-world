"use script";
//Уражнение 1

let a = "100px";
let b = "323px";
let result = parseInt(a) + parseInt(b);
console.log(result);

//Уражнение 2

console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

//Уражнение 3

let c = 123.3399;
console.log(Math.round(c));

let d = 0.111; // Округлить до 1
console.log(Math.ceil(d));

let e = 45.333333; // Округлить до 45.3
console.log(e.toFixed(1));

let f = 3; // Возвести в степень 5 (должно быть 243)
console.log(Math.pow(f, 5));

let g = 4e14; // Записать в сокращенном виде
console.log(g);

let h = "1" == 1; // Поправить условие, чтобы результат был true (значения изменять нельзя, только оператор)
console.log(h);

//Уражнение 4

console.log(0.1 + 0.2 === 0.3); // Вернёт false, почему?
/* 
Потому что JavaScript всегда хранит числа как числа с плавающей запятой двойной точности.
При сложении 0.1 и 0.2 появляются остатки, которые переносятся, когда число конвертируется из 2ичного представления
в 10тичное. Которые в свою очередь не равны остаткам при переводе 0.3 в 2ичную запись и обратно.
*/
console.log((0.1).toFixed(20));
console.log((0.2).toFixed(20));
console.log((0.3).toFixed(20));
let sum = 0.1 + 0.2;
console.log(+sum.toFixed(2) === 0.3); // Вернёт  true
